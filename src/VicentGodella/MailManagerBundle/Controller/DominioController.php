<?php

namespace VicentGodella\MailManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use VicentGodella\MailManagerBundle\Form\DomainType;
use VicentGodella\MailManagerBundle\Entity\Domain;



class DominioController extends Controller
{
    public function indexAction()
    {
		$em = $this->get('doctrine')->getEntityManager();
        $dominios = $em->getRepository('MailManagerBundle:Domain')->findAll();
        
        $request = $this->get('request');
        
        // Petición AJAX
        if ($request->isXmlHttpRequest()) 
        {
            return $this->render('MailManagerBundle:Dominio:ajax_index.html.twig', array(
				'dominios' => $dominios
            ));
        } else { // No es una petición AJAX 
			return $this->render('MailManagerBundle:Dominio:index.html.twig', array(
				'dominios' => $dominios
            ));
        }
    }
	
    public function listAction()
    {
		$em = $this->get('doctrine')->getEntityManager();
        $dominios = $em->getRepository('MailManagerBundle:Domain')->findAll();
        
        $request = $this->get('request');
        return $this->render('MailManagerBundle:Dominio:_list.html.twig', array(
			'dominios' => $dominios
		));
    }
	
	public function newAction()
	{
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();
        $domain = new Domain();
                
        $form = $this->get('form.factory')->create(new DomainType(), $domain);
		
        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);

            if ($form->isValid()) {
                
                // Mensaje para notificar al usuario que todo ha salido bien
             //   $request->getSession()->setFlash('notice', 'El dominio ha sido creado satisfactoriamente');
                
                $em->persist($domain);
                $em->flush();

				$dominios = $em->getRepository('MailManagerBundle:Domain')->findAll();
				return $this->render('MailManagerBundle:Dominio:_list.html.twig', array(
					'dominios' => $dominios
				));
            }
        }
         // Petición AJAX
        if ($request->isXmlHttpRequest()) 
        {
			return $this->render('MailManagerBundle:Dominio:ajax_new.html.twig', array('form' => $form->createView(),
				'domain' => $domain));
        }
		
        return $this->render('MailManagerBundle:Dominio:new.html.twig', array('form' => $form->createView(),
            'domain' => $domain));
	}
	
    public function deleteAction($id)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $domain = $em->getRepository('MailManagerBundle:Domain')->find($id);
        
        $em->remove($domain);
        $em->flush();
        
        return new Response('Eliminado');
    }
}
