<?php

namespace VicentGodella\MailManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('dominio_index', array()));
		
    }
}
