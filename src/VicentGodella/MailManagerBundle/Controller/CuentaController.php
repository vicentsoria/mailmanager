<?php

namespace VicentGodella\MailManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use VicentGodella\MailManagerBundle\Form\UserType;
use VicentGodella\MailManagerBundle\Entity\User;
use VicentGodella\MailManagerBundle\Entity\Domain;



class CuentaController extends Controller
{
    public function indexAction()
    {
		$em = $this->get('doctrine')->getEntityManager();
        $cuentas = $em->getRepository('MailManagerBundle:User')->findAll();
        
        $request = $this->get('request');
        
        // Petición AJAX
        if ($request->isXmlHttpRequest()) 
        {
            return $this->render('MailManagerBundle:Cuenta:ajax_index.html.twig', array(
				'cuentas' => $cuentas
            ));
        } else { // No es una petición AJAX 
			return $this->render('MailManagerBundle:Cuenta:index.html.twig', array(
				'cuentas' => $cuentas
            ));
        }
    }
	
    public function listAction()
    {
		$em = $this->get('doctrine')->getEntityManager();
        $cuentas = $em->getRepository('MailManagerBundle:User')->findAll();
        
        $request = $this->get('request');
        return $this->render('MailManagerBundle:Cuenta:_list.html.twig', array(
			'cuentas' => $cuentas
		));
    }
	
	public function newAction()
	{
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();
        $cuenta = new User();
                
        $form = $this->get('form.factory')->create(new UserType(), $cuenta);
		
        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);

            if ($form->isValid()) {
                $cuenta->setMaildir($cuenta->getId().'/');
                $cuenta->setCrypt(crypt($cuenta->getClear()));
				
                $em->persist($cuenta);
                $em->flush();

				$cuentas = $em->getRepository('MailManagerBundle:User')->findAll();
				return $this->render('MailManagerBundle:Cuenta:_list.html.twig', array(
					'cuentas' => $cuentas
				));
            }
        }
         // Petición AJAX
        if ($request->isXmlHttpRequest()) 
        {
			return $this->render('MailManagerBundle:Cuenta:ajax_new.html.twig', array('form' => $form->createView(),
				'cuenta' => $cuenta));
        }
		
        return $this->render('MailManagerBundle:Cuenta:new.html.twig', array('form' => $form->createView(),
            'cuenta' => $cuenta));
	}
	
    public function deleteAction($id)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $domain = $em->getRepository('MailManagerBundle:User')->find($id);
        
        $em->remove($domain);
		
		$aliases = $em->getRepository('MailManagerBundle:Alias')->findByMail($id);
		
		foreach($aliases as $alias){
			$em->remove($alias);
		}
		
        $em->flush();
        
        return new Response('Eliminado');
    }
}
