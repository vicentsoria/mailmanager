<?php

namespace VicentGodella\MailManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VicentGodella\MailManagerBundle\Entity\Domain
 *
 * @ORM\Table(name="domains")
 * @ORM\Entity
 */
class Domain
{
    /**
     * @var smallint $pkid
     *
     * @ORM\Column(name="pkid", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pkid;

    /**
     * @var string $domain
     *
     * @ORM\Column(name="domain", type="string", length=120, nullable=false)
     */
    private $domain;

    /**
     * @var string $transport
     *
     * @ORM\Column(name="transport", type="string", length=120, nullable=false)
     */
    private $transport = "virtual:";

    /**
     * @var boolean $enabled
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled = true;



    /**
     * Get pkid
     *
     * @return smallint 
     */
    public function getPkid()
    {
        return $this->pkid;
    }

    /**
     * Set domain
     *
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set transport
     *
     * @param string $transport
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;
    }

    /**
     * Get transport
     *
     * @return string 
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}