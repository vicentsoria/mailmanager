<?php

namespace VicentGodella\MailManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VicentGodella\MailManagerBundle\Entity\User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class User
{
    /**
     * @var string $id
     *
     * @ORM\Column(name="id", type="string", length=128, nullable=false)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var smallint $uid
     *
     * @ORM\Column(name="uid", type="smallint", nullable=false)
     */
    private $uid=5000;

    /**
     * @var smallint $gid
     *
     * @ORM\Column(name="gid", type="smallint", nullable=false)
     */
    private $gid=5000;

    /**
     * @var string $home
     *
     * @ORM\Column(name="home", type="string", length=255, nullable=false)
     */
    private $home='/var/spool/mail/virtual';

    /**
     * @var string $maildir
     *
     * @ORM\Column(name="maildir", type="string", length=255, nullable=false)
     */
    private $maildir;

    /**
     * @var boolean $enabled
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled=true;

    /**
     * @var boolean $changePassword
     *
     * @ORM\Column(name="change_password", type="boolean", nullable=false)
     */
    private $changePassword=false;

    /**
     * @var string $clear
     *
     * @ORM\Column(name="clear", type="string", length=128, nullable=false)
     */
    private $clear;

    /**
     * @var string $crypt
     *
     * @ORM\Column(name="crypt", type="string", length=128, nullable=false)
     */
    private $crypt;

    /**
     * @var string $quota
     *
     * @ORM\Column(name="quota", type="string", length=255, nullable=false)
     */
    private $quota = "";

    /**
     * @var string $procmailrc
     *
     * @ORM\Column(name="procmailrc", type="string", length=128, nullable=false)
     */
    private $procmailrc = "";

    /**
     * @var string $spamassassinrc
     *
     * @ORM\Column(name="spamassassinrc", type="string", length=128, nullable=false)
     */
    private $spamassassinrc = "";


    /**
     * Set id
     *
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set uid
     *
     * @param smallint $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * Get uid
     *
     * @return smallint 
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set gid
     *
     * @param smallint $gid
     */
    public function setGid($gid)
    {
        $this->gid = $gid;
    }

    /**
     * Get gid
     *
     * @return smallint 
     */
    public function getGid()
    {
        return $this->gid;
    }

    /**
     * Set home
     *
     * @param string $home
     */
    public function setHome($home)
    {
        $this->home = $home;
    }

    /**
     * Get home
     *
     * @return string 
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set maildir
     *
     * @param string $maildir
     */
    public function setMaildir($maildir)
    {
        $this->maildir = $maildir;
    }

    /**
     * Get maildir
     *
     * @return string 
     */
    public function getMaildir()
    {
        return $this->maildir;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set changePassword
     *
     * @param boolean $changePassword
     */
    public function setChangePassword($changePassword)
    {
        $this->changePassword = $changePassword;
    }

    /**
     * Get changePassword
     *
     * @return boolean 
     */
    public function getChangePassword()
    {
        return $this->changePassword;
    }

    /**
     * Set clear
     *
     * @param string $clear
     */
    public function setClear($clear)
    {
        $this->clear = $clear;
    }

    /**
     * Get clear
     *
     * @return string 
     */
    public function getClear()
    {
        return $this->clear;
    }

    /**
     * Set crypt
     *
     * @param string $crypt
     */
    public function setCrypt($crypt)
    {
        $this->crypt = $crypt;
    }

    /**
     * Get crypt
     *
     * @return string 
     */
    public function getCrypt()
    {
        return $this->crypt;
    }

    /**
     * Set quota
     *
     * @param string $quota
     */
    public function setQuota($quota)
    {
        $this->quota = $quota;
    }

    /**
     * Get quota
     *
     * @return string 
     */
    public function getQuota()
    {
        return $this->quota;
    }

    /**
     * Set procmailrc
     *
     * @param string $procmailrc
     */
    public function setProcmailrc($procmailrc)
    {
        $this->procmailrc = $procmailrc;
    }

    /**
     * Get procmailrc
     *
     * @return string 
     */
    public function getProcmailrc()
    {
        return $this->procmailrc;
    }

    /**
     * Set spamassassinrc
     *
     * @param string $spamassassinrc
     */
    public function setSpamassassinrc($spamassassinrc)
    {
        $this->spamassassinrc = $spamassassinrc;
    }

    /**
     * Get spamassassinrc
     *
     * @return string 
     */
    public function getSpamassassinrc()
    {
        return $this->spamassassinrc;
    }
}