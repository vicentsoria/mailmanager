<?php

namespace VicentGodella\MailManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VicentGodella\MailManagerBundle\Entity\Alias
 *
 * @ORM\Table(name="aliases")
 * @ORM\Entity
 */
class Alias
{
    /**
     * @var smallint $pkid
     *
     * @ORM\Column(name="pkid", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pkid;

    /**
     * @var string $mail
     *
     * @ORM\Column(name="mail", type="string", length=120, nullable=false)
     */
    private $mail;

    /**
     * @var string $destination
     *
     * @ORM\Column(name="destination", type="string", length=120, nullable=false)
     */
    private $destination;

    /**
     * @var boolean $enabled
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;



    /**
     * Get pkid
     *
     * @return smallint 
     */
    public function getPkid()
    {
        return $this->pkid;
    }

    /**
     * Set mail
     *
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set destination
     *
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * Get destination
     *
     * @return string 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}