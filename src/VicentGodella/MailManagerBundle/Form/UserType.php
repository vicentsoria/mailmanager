<?php

namespace VicentGodella\MailManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;


class UserType extends AbstractType
{
    public function getName()
    {
        return 'User';
    }
    
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('id', 'email', array('label' => 'Email'));
        $builder->add('name', null, array('label' => 'Nombre'));
		
		$builder->add('clear', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'The password fields must match.'
        ));	
		
        $builder->add('enabled', null, array('label' => '¿Habilitada?'));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'VicentGodella\MailManagerBundle\Entity\User',
        );
    }

}