<?php

namespace VicentGodella\MailManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;


class DomainType extends AbstractType
{
    public function getName()
    {
        return 'Domain';
    }
    
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('domain', null, array('label' => 'Dominio'));
        $builder->add('enabled', null, array('label' => '¿Habilitado?', ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'VicentGodella\MailManagerBundle\Entity\Domain',
        );
    }

}